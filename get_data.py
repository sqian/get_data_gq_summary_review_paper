import os
import glob
import re
import itertools

import math

ref_data = {"13": "./ZPrime_13TeV_gq0p25.dat","8": "./ZPrime_8TeV_gq0p25.dat"}

quark_masses = {
    "u":2.3e-3,
    "d":4.8e-3,
    "s":0.095,
    "c":1.275,
    "b":4.180,
    "t":173.210
}

def Gammaqq(gq,Mmed,qflavor,style):
    if quark_masses[qflavor] > Mmed / 2.:
        return 0.
    elif style=="Axial":
        return gq**2*Mmed*math.pow((1-float(4*quark_masses[qflavor]**2)/Mmed**2),1.5)/(4*pi)
    elif style=="Vector":
        return gq**2*Mmed*math.pow((1-float(4*quark_masses[qflavor]**2)/Mmed**2),0.5)*(1+2*float(quark_masses[qflavor]**2)/Mmed**2)/(4*pi)

def Gammaqq_tot(gq,Mmed,style):
    return sum([Gammaqq(gq, Mmed, qflavor, style) for qflavor in ["u", "d", "s", "c", "b", "t"]])
    #if Mmed>=173*2:
    #    return 5*Gammaqq(gq,Mmed,0,style)+Gammaqq(gq,Mmed,173,style)
    #else:
    #    return 5*Gammaqq(gq,Mmed,0,style)


def read_csv(file_name):
    with open(file_name, "r") as f:
        lines = f.readlines()
    mass = []
    obs = []
    header = lines[0].replace("#","").strip().split()
    assert len(header) == 2, f"Expected 2 columns in the header, got {len(header)}"
    assert header[0].startswith("m"), f"Expected first column to be mass, got {header[0]}"
    scenario = header[1]
    if scenario == "gq":
        for line in lines[1:]:
            if scenario == "gq":
                if line.startswith("#"):
                    continue
                line = line.strip()
                if line == "":
                    continue
                line = line.split()
                mass.append(float(line[0]))
                obs.append(float(line[1]))

    elif scenario == "gq_av":
        for line in lines[1:]:
            if line.startswith("#"):
                continue 
            line_content = line.strip()
            if line_content == "":
                continue
            line_contents = line_content.split()
            mZp = float(line_contents[0])
            gq_av = float(line_contents[1])
            # Gammaqq_tot: Gammaqq_tot(gq,Mmed,style):
            # Gammaqq(gq,Mmed,qflavor,style):
            num = sum([Gammaqq(0.25, mZp, qflavor, "Axial") for qflavor in ["u", "d"]]) * sum([Gammaqq(0.25, mZp, qflavor, "Axial") for qflavor in ["u", "d", "s", "c", "b"]]) / Gammaqq_tot(0.25, mZp, "Axial")
            den = sum([Gammaqq(0.25, mZp, qflavor, "Vector") for qflavor in ["u", "d"]]) * sum([Gammaqq(0.25, mZp, qflavor, "Vector") for qflavor in ["u", "d", "s", "c", "b"]]) / Gammaqq_tot(0.25, mZp, "Vector")
            gq_v = gq_av * math.sqrt(num / den)
            #gq_v = gq_av * sqrt(sum([Gammaqq(0.25, mZp, qflavor, "Axial") for qflavor in ["u", "d"]]) / sum([Gammaqq(0.25, mZp, qflavor, "Vector") for qflavor in ["u", "d"]]))
            mass.append(mZp)
            obs.append(float(gq_v))

    elif "xs" in scenario:
        re_xs = re.compile("xs(?P<sqrts>\d+)")
        re_xs_result = re_xs.search(scenario)
        assert re_xs_result, f"Could not find sqrts in {scenario}"
        sqrts = float(re_xs_result.group("sqrts"))

		# Extract xses from txt file
        xses = []
        for line in lines[1:]:
            if line.startswith("#"):
                continue
            line_content = line.strip()
            if line_content == "":
                continue
            line_contents = line_content.split()
            mass.append(float(line_contents[0]))
            xses.append(float(line_contents[1]))
            

        # Load reference xses
        reference_mass_xs = []
        with open(ref_data[str(int(sqrts))], "r") as f:
            for line in f:
                if line[0] == "#":
                    continue
                line_contents = line.split()
                reference_mass_xs.append((float(line_contents[0]), float(line_contents[1])))
        reference_mass_xs.sort(key=lambda x: x[0])
        #convert to dict
        reference_mass_xs_dict = {x[0]: x[1] for x in reference_mass_xs}
        # reference_xs_graph = TGraph(len(reference_mass_xs))
        # for i, mass_xs in enumerate(reference_mass_xs):
        #     reference_xs_graph.SetPoint(i, mass_xs[0], mass_xs[1])

        # Do conversions
        # for i, mass in enumerate(self._masses):
        #     self._gqs.append(self._reference_xses[sqrts]["gq0"] * sqrt(xses[i] / reference_xs_graph.Eval(mass)))

        # self._data_loaded = True
        # self.create_graph()
        #self.print_gq()

        gq0 = 0.25
        for i in range(len(mass)):
            obs.append(gq0 * math.sqrt(xses[i] / reference_mass_xs_dict[mass[i]]))

    return {"mass": mass.copy(), "limit": obs.copy()}

# needed_analyses = [
#     "EXO16056_narrow_lowmass_obs",
# "EXO14005_obs",
# "EXO19004_obs",
# "EXO17027_obs",
# "EXO18012_AK8_obs",
# "EXO16057_SR1_obs",
# # "EXO16057_SR2_obs",
# "EXO16056_narrow_highmass_obs",
# "EXO19012_obs",
# "EXO16046_obs"
# ]

needed_analyses = {
        "EXO16056_narrow_lowmass_obs" : ["EXO16056_narrow_lowmass_obs"],
"EXO14005_obs" : ["EXO14005_obs"],
"EXO19004_obs" : ["EXO19004_obs"],
"EXO17027_obs" : ["EXO17027_obs"],
"EXO18012_AK8_obs" : ["EXO18012_AK8_obs", "EXO18012_CA15_obs"],
"EXO16057_SR1_obs" : ["EXO16057_SR1_obs", "EXO16057_SR2_obs"],
"EXO16056_narrow_highmass_obs" : ["EXO16056_narrow_highmass_obs"],
"EXO19012_obs"  : ["EXO19012_obs"],
"EXO16046_obs" : ["EXO16046_obs"]
}
data_dir = "../gq_summary/GQSummary/data" # Change this to the correct path

legend_entries_origin = {
	"EXO16046_obs":"#splitline{Dijet #chi #it{#scale[1.]{(EPJC 78, 789, EPJC 82, 379 (erratum))}}}{#lower[-0.1]{35.9 fb^{-1}, 13 TeV}}",
	"EXO16056_narrow_obs":"#splitline{Dijet #scale[1.]{#it{(JHEP 2018, 130)}}}{#lower[-0.1]{27 fb^{-1}, 13 TeV}}",
	"EXO16056_narrow_lowmass_obs":"#splitline{#bf{Dijet scouting} #scale[1.]{#it{(JHEP 2018, 130)}}}{#lower[-0.1]{27 fb^{-1}, 13 TeV}}",
	"EXO16056_narrow_highmass_obs":"#splitline{Dijet #scale[1.]{#it{(JHEP 2018, 130)}}}{#lower[-0.1]{35.9 fb^{-1}, 13 TeV}}",
	"EXO16056_wide_obs":"#splitline{Broad Dijet #it{[arXiv:1806.00843]}}{#lower[-0.1]{35.9 fb^{-1}, 13 TeV}}",
	"EXO16057_SR1_obs":"#splitline{Dijet b-tagged #scale[1.]{#it{(PRL 120, 201801)}}}{#lower[-0.1]{19.7 fb^{-1}, 8 TeV}}",
	"EXO17001_obs":"#splitline{Boosted Dijet #it{[arXiv:1710.00159]}}{#lower[-0.1]{35.9 fb^{-1}, 13 TeV}}",
	"EXO18012_AK8_obs":"#splitline{Boosted Dijet #scale[1.]{#it{(PRD 100, 112007)}}}{#lower[-0.1]{77.0 fb^{-1}, 13 TeV}}",
	"EXO14005_obs":"#splitline{#bf{Dijet scouting} #scale[1.]{#it{(PRL 117, 031802)}}}{#lower[-0.1]{19.7 fb^{-1}, 8 TeV}}",
	"EXO17026_obs":"#splitline{Dijet #it{[EXO-17-026]}}{#lower[-0.1]{77.8 fb^{-1}, 13 TeV}}",
	"EXO17027_obs":"#splitline{Boosted Dijet+#gamma #scale[1.]{#it{(PRL 123, 231803)}}}{#lower[-0.1]{35.9 fb^{-1}, 13 TeV}}",
	"EXO19012_obs":"#splitline{Dijet #scale[1.]{#it{(JHEP 2020, 033)}}}{#lower[-0.1]{137 fb^{-1}, 13 TeV}}",
	"EXO19004_obs":"#splitline{#bf{Dijet+ISR sc.} #scale[1.]{#it{(PLB 805, 135448)}}}{18.3 fb^{-1}, 13 TeV}",
}

legend_entries_new = {
    'EXO16046_obs': r'Dijet $\chi$, (EPJC 78, 789) \n $\sqrt{s}$ = 13 TeV, 35.9 fb$^{-1}$',
    'EXO16056_narrow_obs': r'Dijet $\chi$, (JHEP 2018, 130) \n $\sqrt{s}$ = 13 TeV, 27 fb$^{-1}$',
    'EXO16056_narrow_lowmass_obs': r'Dijet \textbf{Scouting}, (JHEP 2018, 130) \n $\sqrt{s}$ = 13 TeV, 27 fb$^{-1}$',
    'EXO16056_narrow_highmass_obs': r'Dijet $\chi$, (JHEP 2018, 130) \n $\sqrt{s}$ = 13 TeV, 35.9 fb$^{-1}$',
    'EXO16056_wide_obs': r'Broad Dijet [arXiv:1806.00843] \n $\sqrt{s}$ = 13 TeV, 35.9 fb$^{-1}$',
    'EXO16057_SR1_obs': r'Dijet b-tagged (PRL 120, 201801) \n $\sqrt{s}$ = 8 TeV, 19.7 fb$^{-1}$',
    'EXO17001_obs': r'Boosted Dijet [arXiv:1710.00159] \n $\sqrt{s}$ = 13 TeV, 35.9 fb$^{-1}$',
    'EXO18012_AK8_obs': r'Boosted Dijet (PRD 100, 112007) \n $\sqrt{s}$ = 13 TeV, 77.0 fb$^{-1}$',
    'EXO14005_obs': r'Dijet \textbf{Scouting} (PRL 117, 031802) \n $\sqrt{s}$ = 8 TeV, 19.7 fb$^{-1}$',
    'EXO17026_obs': r'Dijet [EXO-17-026] \n $\sqrt{s}$ = 13 TeV, 77.8 fb$^{-1}$',
    'EXO17027_obs': r'Boosted Dijet+$\gamma$ (PRL 123, 231803) \n $\sqrt{s}$ = 13 TeV, 35.9 fb$^{-1}$',
    'EXO19012_obs': r'Dijet (JHEP 2020, 033) \n $\sqrt{s}$ = 13 TeV, 137 fb$^{-1}$',
    'EXO19004_obs': r'Dijet+ISR \textbf{Scouting} (PLB 805, 135448) \n $\sqrt{s}$ = 13 TeV, 18.3 fb$^{-1}$',
}

result = {}

for ana in needed_analyses:
    # full_path = os.path.join(data_dir, f"{ana}.dat")
    ana_list = []
    for ana_file in needed_analyses[ana]:
        full_path = os.path.join(data_dir, f"{ana_file}.dat")
        assert os.path.exists(full_path), f"File {full_path} does not exist"
        assert ana in legend_entries_new, f"Analysis {ana} not in legend_entries_new"
        ana_list.append(read_csv(full_path))
    
    tmp_dict = {}
    tmp_dict['mass'] = list(itertools.chain(*[x['mass'] for x in ana_list]))
    tmp_dict['limit'] = list(itertools.chain(*[x['limit'] for x in ana_list]))
    result[ana] = tmp_dict
    result[ana]["label"] = legend_entries_new[ana]
    

import pprint
pprint.pprint(result)

# save the result to a file
import pickle
with open("data.pkl", "wb") as f:
    pickle.dump(result, f)

import json
with open("data.json", "w") as f:
    json.dump(result, f)
