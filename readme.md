# How to use

Just make a directory and update the relative path to the `gq_summary` directory

The legend entries are made to be of `Name (Journal Ref) \n C.O.M., lumi`

I assume the bold face is done by using `\textbf{[scouting]}`
